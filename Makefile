all: кошкин-дом.mp4 три-поросёнка.mp4 доктор-айболит.mp4

# Commands from superuser.com
# https://superuser.com/questions/1041816/combine-one-image-one-audio-file-to-make-one-video-using-ffmpeg/1041818#1041818

# This makes a 577 MB file, too big.
video.mkv: audio.mp3 cover.jpeg
	ffmpeg -loop 1 -i cover.jpeg -i audio.mp3 -shortest -acodec copy -vcodec mjpeg video.mkv

# Just 28 MB, okay'ish
%.mp4: %.mp3 %.jpeg
	ffmpeg -loop 1 -i ${@:.mp4=.jpeg} -i ${@:.mp4=.mp3} -c:v libx264 -tune stillimage -c:a aac -b:a 192k -pix_fmt yuv420p -shortest $@
